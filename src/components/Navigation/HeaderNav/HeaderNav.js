import React, {useEffect, useState} from 'react';
import {NavLink} from "react-router-dom";
import './HeaderNav.css';
import axiosApi from "../../../axiosApi";
import withLoader from "../../../hoc/withLoader";

const HeaderNav = () => {
	const [pages, setPages] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get('/pages.json');
			const fetchedPages = Object.keys(response.data).map(id => {
				const page = response.data[id];

				return {...page, id}
			});
			setPages(fetchedPages);
		};

		fetchData().catch(console.error);
	}, []);

	return (
		<header className="header">
			<h2>Static Pages</h2>
			<nav className="headerNav">
				<ul>
					{pages.map(page => (
						<li key={page.id}>
							<NavLink to={'/pages/' + page.id}>
								{page.id.charAt(0).toUpperCase() + page.id.slice(1)}
							</NavLink>
						</li>
					))}
					<li>
						<NavLink to='/pages/admin'>
							Admin
						</NavLink>
					</li>
				</ul>
			</nav>
		</header>
	);
};

export default withLoader(HeaderNav, axiosApi);