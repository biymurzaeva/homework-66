import axios from "axios";

const axiosApi = axios.create({
	baseURL: 'https://work-project-js-default-rtdb.firebaseio.com'
});

export default axiosApi;