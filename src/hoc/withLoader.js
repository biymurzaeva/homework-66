import React, {useEffect, useMemo, useState} from 'react';
import Loader from "../Loader/Loader";

const withLoader = (WrappedComponent, axios) => {
	return function LoaderHOC (props) {
		const [loading, setLoading] = useState(null);

		 const icId = useMemo(() => {
		 	 axios.interceptors.request.use(
					req => {
						setLoading(true);
						return req;
					},
					error => {
						throw error;
					}
				);

		 	 return axios.interceptors.response.use(
					res => {
						setLoading(false);
						return res;
					},
					error => {
						throw error;
					}
				);

		  }, []);

		useEffect(() => {
			return () => {
				axios.interceptors.response.eject(icId);
			}
		}, [icId]);


		return (
			<>
				{loading && <Loader/>}
				<WrappedComponent {...props}/>
			</>
		)
	};
};

export default withLoader;