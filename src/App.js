import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Page from "./containers/Page/Page";
import EditPage from "./containers/EditPage/EditPage";

const App = () => (
  <BrowserRouter>
    <Layout>
      <Switch>
        <Route path="/" exact render={() => <h1>Welcome</h1>}/>
        <Route path="/pages/admin" component={EditPage}/>
        <Route path="/pages/:page" component={Page}/>
      </Switch>
    </Layout>
  </BrowserRouter>
);

export default App;
