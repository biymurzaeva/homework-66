import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './EditPage.css';
import withLoader from "../../hoc/withLoader";

const EditPage = ({history}) => {
	const [page, setPage] = useState({
		title: '',
		content: ''
	});
	const [selected, setSelected] = useState('about');

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get('/pages/' + selected + '.json');
			setPage(response.data);
		};

		fetchData().catch(console.error);
	}, [selected]);

	const onInputChange = e => {
		const {name, value} = e.target;

		setPage(prev => ({
			...prev,
			[name]: value
		}));
	};

	const editPage = async e => {
		e.preventDefault();

		try {
			await axiosApi.put('/pages/' + selected + '.json', page);
		} finally {
			history.push('/pages/' + selected);
		}
	};

	return (
		<div className="form-block">
			<h2>Edit pages</h2>
			<form onSubmit={editPage}>
				<p>Select page</p>
				<select name="pages" value={selected} onChange={e => setSelected(e.currentTarget.value)}>
					<option value="about">About</option>
					<option value="home">Home</option>
					<option value="contacts">Contacts</option>
					<option value="divisions">Divisions</option>
				</select>
				<div className="form-row">
					<p>Title</p>
					<input type="text" name="title" value={page.title} onChange={onInputChange}/>
				</div>
				<div className="form-row">
					<p>Content</p>
					<textarea rows="8" name="content" value={page.content} onChange={onInputChange}/>
				</div>
				<button type="submit">Save</button>
			</form>
		</div>
	);
};

export default withLoader(EditPage, axiosApi);