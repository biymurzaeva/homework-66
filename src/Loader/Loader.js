import React from 'react';
import spinner from '../assets/images/spinner.gif';
import './Loader.css';

const Loader = () => {
	return (
		<div className="Loader-container">
			<div className="Loader">
				<img src={spinner} alt="Spinner"/>
			</div>
		</div>
	);
};

export default Loader;